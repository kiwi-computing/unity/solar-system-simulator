﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Globalization;

public class Planets : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> planetsList = new List<GameObject>();
    public Material ParticleShader;

    // Center the sun
    private Vector3 SunCoordinates = new Vector3 (0, 0, 0);

    private List<string> stringList = new List<string>();
    private List<string[]> parsedList = new List<string[]>();

    void Start()
    {
        readTextFile();
        CreatePlanets();
    }

    // Read CSV file with all the info about the planets
    void readTextFile()
    {
        StreamReader inp_stm = new StreamReader("Assets/Resources/PlanetInfo.csv");

        while (!inp_stm.EndOfStream)
        {
            stringList.Add(inp_stm.ReadLine());
        }

        inp_stm.Close();

        parseList();
    }

    void parseList()
    {
        for (int i = 0; i < stringList.Count; i++)
        {
            string[] temp = stringList[i].Split(',');
            for (int j = 0; j < temp.Length; j++)
            {
                temp[j] = temp[j].Trim();  //removed the blank spaces
            }
            // List of arrays
            parsedList.Add(temp);
        }
        parsedList.RemoveAt(0);
    }

    // Place the planets in the right orbit
    void PlacePlanets(List<GameObject> planets)
    {
        for (int i = 0; i < planets.Count; i++)
        {
            float xCoordinate;
            string[] planetInfo = parsedList[i];
            float SunRadius = GameObject.Find("Sun(Clone)").GetComponentInChildren<Renderer>().bounds.size.x;
            float planetsRadius = (planets[i].transform.childCount > 0f) ? planets[i].GetComponentInChildren<Renderer>().bounds.size.x : planets[i].GetComponent<Renderer>().bounds.size.x;
            if (planets[i].name != "Sun(Clone)")
            {
                xCoordinate = SunCoordinates.x + SunRadius + planetsRadius + (float.Parse(planetInfo[8], CultureInfo.InvariantCulture) * float.Parse(System.Math.Pow(10, 6).ToString()))/ 10000;
                // Add reflections
                ReflectionProbe reflection = planets[i].AddComponent<ReflectionProbe>();
                reflection.mode = UnityEngine.Rendering.ReflectionProbeMode.Realtime;
                reflection.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.EveryFrame;
                reflection.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.IndividualFaces;
                reflection.size = planets[i].GetComponentInChildren<Renderer>().bounds.size;
            }
            else
            {
                xCoordinate = SunCoordinates.x;
                planets[i].GetComponent<SphereCollider>().radius = 500f;
                planets[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
                // Add light source
                Light light = planets[i].AddComponent<Light>();
                light.range = 1000000f;
                light.intensity = 2;
                //SunEffects(planets[i]);
            }
            //planets[i].transform.position = new Vector3(Mathf.Cos(i * Mathf.PI * 2f / planets.Count) * xCoordinate, 0, Mathf.Sin(i * Mathf.PI * 2f / planets.Count) * xCoordinate);
            planets[i].transform.position = new Vector3(xCoordinate, 0, 0);
        }
    }

    // Create Planets
    void CreatePlanets()
    {
        foreach (string[] planetInfo in parsedList)
        {
            GameObject planet = Instantiate(Resources.Load("Models/"+ planetInfo[0]) as GameObject);
            planet.transform.SetParent(Universe.Instance.transform);
            //planet.AddComponent<ConstantForce>();

            planet.AddComponent<SphereCollider>();
            planet.AddComponent<Rigidbody>();
            planet.AddComponent<Physics>();
            planet.GetComponent<Rigidbody>().useGravity = false;
            planet.GetComponent<Rigidbody>().angularDrag = 0;
            planet.GetComponent<Rigidbody>().mass = (float.Parse(planetInfo[1], CultureInfo.InvariantCulture));
            planet.transform.rotation = Quaternion.Euler((float)0.0, (float) 0.0, float.Parse(planetInfo[21], CultureInfo.InvariantCulture) +90);

            // Scale the planets 10000's of the real size
            double Scale = (double.Parse(planetInfo[2], CultureInfo.InvariantCulture) / 2)/10000;
            planet.transform.localScale = new Vector3(float.Parse(Scale.ToString()), float.Parse(Scale.ToString()), float.Parse(Scale.ToString()));

            planetsList.Add(planet);
        }
        PlacePlanets(planetsList);
    }

    // Add sun effects
    void SunEffects(GameObject sun)
    {
        sun.AddComponent<ParticleSystem>();
        var particleShape = sun.GetComponent<ParticleSystem>().shape;
        var particle = sun.GetComponent<ParticleSystem>().main;
        // Sets the shape to a sphere
        particleShape.shapeType = ParticleSystemShapeType.Sphere;
        // Sets the shapes radius
        particleShape.radius = 550f;
        // Sets the material
        sun.GetComponent<ParticleSystemRenderer>().material = ParticleShader;
        // Removes speed
        particle.startSpeed = 0f;
        // Random size
        particle.startSize = new ParticleSystem.MinMaxCurve(10000f, 15000f);


    }
}
