﻿using UnityEngine;
using System.Collections;

public class MoveView : MonoBehaviour {

	private float speed = Mathf.Pow(10,4);
	private float mouseSensitivity = 5.0f;
	private float rotY = -90f;
	private float scrollSpeed = 10.5f;
	float cameraDistanceMax = 20f;
	float cameraDistanceMin = 5f;
	float cameraDistance = 55.51567f;

	void Update () {
		// Movement
		transform.Translate( new Vector3(
			Input.GetAxis("Horizontal") * speed,
			Input.GetAxis("Vertical") * speed,
			0) * Time.deltaTime );
		// Rotation        
		if (Input.GetMouseButton(1))
		{
			float rotX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * mouseSensitivity;
			rotY += Input.GetAxis("Mouse Y") * mouseSensitivity;
			rotY = Mathf.Clamp(rotY, -89.5f, 89.5f);
			transform.localEulerAngles = new Vector3(-rotY, rotX, 0.0f);
		}
		// Distance
		cameraDistance += Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
		GetComponent<Camera>().fieldOfView = cameraDistance;
	}
}
