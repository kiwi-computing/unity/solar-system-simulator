﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIElements : MonoBehaviour
{
    public GameObject textContainer;
    public float Ratio = 20;

    private List<GameObject> planetName = new List<GameObject>();
    private float _oldWidth;
    private float _oldHeight;
    private float _fontSize = 16;
    
    private void CreateText()
    {
        if(textContainer.GetComponent<TextMeshPro>() != true)
        {
            for (int i = 0; i < Universe.Instance.GetComponent<Planets>().planetsList.Count; i++)
            {
                textContainer = Instantiate(textContainer, gameObject.transform);
                textContainer.AddComponent<TextMeshPro>();
                // Add Planet name over gameobject
                textContainer.GetComponent<TextMeshPro>().text = Universe.Instance.GetComponent<Planets>().planetsList[i].name.Remove(Universe.Instance.GetComponent<Planets>().planetsList[i].name.Length - 7);
                textContainer.GetComponent<TextMeshPro>().alignment = TextAlignmentOptions.Center;
                planetName.Add(textContainer);
            }
        }
        else
        {
            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(planetName.Count != 0)
        {
            for (int i = 0; i < planetName.Count; i++)
            {
                GameObject planet = Universe.Instance.GetComponent<Planets>().planetsList[i];
                TextMeshPro planetText = planetName[i].GetComponent<TextMeshPro>();
                // Move text container with planet
                planetText.transform.position = planet.transform.position;
                if (_oldWidth != Screen.width || _oldHeight != Screen.height)
                {
                    _oldWidth = Screen.width;
                    _oldHeight = Screen.height;
                    _fontSize = Mathf.Min(Screen.width, Screen.height) * Ratio;
                }
                Scale(planetText, planetName[i]);
            }
        }
        else
        {
            CreateText();
        }
    }

    private void Scale(TextMeshPro text, GameObject planet)
    {
        text.fontSize = _fontSize;
        planet.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width*Ratio, Screen.width*Ratio);

    }
}
