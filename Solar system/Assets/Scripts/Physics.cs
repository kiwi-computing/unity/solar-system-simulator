﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Physics : MonoBehaviour
{
    private static readonly float G = (float)6.67428 * Mathf.Pow(10, -11);

    private void Start()
    {
        for (int i = 0; i < Universe.Instance.GetComponent<Planets>().planetsList.Count; i++)
        {
            GameObject planet = Universe.Instance.GetComponent<Planets>().planetsList[i];
            if (planet.name != name)
            {
                // Find distance betweens objects
                Vector3 direction = transform.position - planet.transform.position;
                double distance = direction.magnitude;
                if (distance == 0f)
                {
                    return;
                }
                Vector3 force = new Vector3(0, 0, Mathf.Sqrt(((G * planet.GetComponent<Rigidbody>().mass) / (float)distance) * Universe.Instance.Multipler));
                GetComponent<Rigidbody>().velocity = force * transform.position.normalized.x;
                //Debug.Log(GetComponent<Rigidbody>().velocity);
                //Debug.DrawLine(transform.position, (force * transform.position.normalized.x), Color.red, Mathf.Pow(10,19));
            }
        }
    }
    private void FixedUpdate()
    {
        for (int i = 0; i < Universe.Instance.GetComponent<Planets>().planetsList.Count; i++)
        {
            GameObject planet = Universe.Instance.GetComponent<Planets>().planetsList[i];
            if (planet.name != name)
            {
                // Find distance betweens objects
                Vector3 direction = planet.transform.position - transform.position;
                double distance = direction.magnitude;
                // Fixed error on objects on same position
                if (distance == 0f)
                {
                    return;
                }
                Vector3 forceAcceleration = Acceleration(distance,direction,planet);
                GetComponent<Rigidbody>().AddForce((forceAcceleration) * Universe.Instance.Multipler, ForceMode.Acceleration);
                Debug.DrawLine(transform.position, (forceAcceleration) * Universe.Instance.Multipler, Color.white, 0.2f);
                
            }
        }
    }

    // Calculates the acceleration and makes it into a force
    private Vector3 Acceleration(double distance, Vector3 direction, GameObject obj)
    {
        double acceleration = (G * obj.GetComponent<Rigidbody>().mass) / Mathf.Pow((float)distance, 2);
        return (direction.normalized * (float)acceleration);
    }

    // Calculates the cross product for the vectors
    private Vector3 CrossProduct(Vector3 gravity, Vector3 acceleration) => Vector3.Cross(gravity, acceleration);
}
