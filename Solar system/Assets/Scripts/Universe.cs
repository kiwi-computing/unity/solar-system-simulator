﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Universe : MonoBehaviour
{
    public static Universe Instance;
    public float Multipler;

    // Keeps the gameObject this script is attached to alive and carry it over to the other scenes
    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        GetComponent<Planets>();
    }

}
