﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackground : MonoBehaviour
{
 
    // Update is called once per frame
    void Update()
    {
        Background();
    }

    // Updates background
    void Background ()
    {
        // Creates a 3D effect on 2D images
        float parralaxBack = 15f;
        float parralaxFront = 10f;

        // Gets Childen game objects
        GameObject Back = gameObject.transform.GetChild(0).gameObject;
        GameObject Front = gameObject.transform.GetChild(1).gameObject;

        // Gets the component Mesh Render
        MeshRenderer backMR = Back.GetComponent<MeshRenderer>();
        MeshRenderer frontMR = Front.GetComponent<MeshRenderer>();

        // Gets a copy of materials
        Material matBack = frontMR.material;
        Material matFront = backMR.material;

        Vector2 offsetBack = matBack.mainTextureOffset;
        Vector2 offsetFront = matFront.mainTextureOffset;

        // Moves stars relative to the position, scale and parralax effect
        offsetBack.x = transform.position.x / transform.localScale.x / parralaxBack;
        offsetBack.y = transform.position.y / transform.localScale.y / parralaxBack;
        offsetFront.x = transform.position.x / transform.localScale.x / parralaxFront;
        offsetFront.y = transform.position.y / transform.localScale.y / parralaxFront;

        matBack.mainTextureOffset = offsetBack;
        matFront.mainTextureOffset = offsetFront;
    }
}
